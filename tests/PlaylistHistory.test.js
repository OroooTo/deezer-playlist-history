const Apify = require('apify');
const nock = require('nock');
const path = require('path');

const PlaylistHistory = require('../src/PlaylistHistory');

const tu = require('./testUtilities');

describe('PlaylistHistory', () => {

  beforeEach(() => {
  });

  describe('#run', () => {

    test('workflow', async () => {
      // run #1: first run
      // run #2: nothing has changed
      // run #3: tracks have been added and removed

      // TODO: test with an email
      // mocking 'apify' mocks all the Apify functions...
      // const playlistHistory = new PlaylistHistory(12345, 'test@example.com');
      const playlistHistory = new PlaylistHistory(12345);

      const dateIsoRegEx = /^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}Z$/;
      const storeNameRegEx = /^deezer-playlist-history--12345--\d+$/;

      const track1 = tu.newPlaylistTrackAsJSON(1, 101, 201);
      const track2 = tu.newPlaylistTrackAsJSON(2, 101, 201);
      const track3 = tu.newPlaylistTrackAsJSON(3, 101, 202);
      const track4 = tu.newPlaylistTrackAsJSON(4, 101, 202);

      // ----------------------------------------------- run #1: first run -----
      mockDeezerTracksRequest('run-01.json');
      const beforeRun1 = new Date();
      await playlistHistory.run();
      const afterRun1 = new Date();

      const expectedMetadataItem1 = {
        storeVersion: 1,
        takenAt: {
          timestamp: expect.any(Number),
          iso: expect.stringMatching(dateIsoRegEx)
        },
        snapshot: {
          id: 12345,
          takenAt: {
            timestamp: expect.any(Number),
            iso: expect.stringMatching(dateIsoRegEx)
          },
          storeName: expect.stringMatching(storeNameRegEx),
          tracksCount: 3,
          tracksHash: '55b84a9d317184fe61224bfb4a060fb0'
        },
        delta: {
          withSnapshot: null,
          addedTracksCount: 3,
          removedTracksCount: 0
        }
      };
      const expectedMetadata1 = {
        items: [expectedMetadataItem1],
        total: 1,
        offset: 0,
        count: 1,
        limit: 999999999999
      };
      // check the general structure of the response
      const actualMetadata1 = await loadMetadata();
      expect(actualMetadata1).toMatchObject(expectedMetadata1);

      const actualMetadata1i1 = actualMetadata1['items'][0];
      // check that the date is consistent with the call
      expect(actualMetadata1i1['takenAt']['timestamp']).toBeGreaterThanOrEqual(beforeRun1.getTime());
      expect(actualMetadata1i1['takenAt']['timestamp']).toBeLessThanOrEqual(afterRun1.getTime());
      // check that the metadata date is the same than the snapshot one
      expect(actualMetadata1i1['takenAt']['timestamp']).toStrictEqual(actualMetadata1i1['snapshot']['takenAt']['timestamp']);

      const actualData1 = await loadSnaphot(actualMetadata1i1);
      // check the snapshot data
      expect(actualData1).toStrictEqual({
        items: [track1, track2, track3],
        total: 3,
        offset: 0,
        count: 3,
        limit: 999999999999
      });

      // TODO: check that no email was sent
      // mocking 'apify' mocks all the Apify functions...
      // expect(Apify.call).not.toHaveBeenCalled();

      // ------------------------------------- run #2: nothing has changed -----
      mockDeezerTracksRequest('run-02.json');
      const beforeRun2 = new Date();
      await playlistHistory.run();
      const afterRun2 = new Date();

      const expectedMetadataItem2 = {
        storeVersion: 1,
        takenAt: {
          timestamp: expect.any(Number),
          iso: expect.stringMatching(dateIsoRegEx)
        },
        snapshot: {
          id: 12345,
          takenAt: {
            timestamp: expect.any(Number),
            iso: expect.stringMatching(dateIsoRegEx)
          },
          storeName: actualMetadata1i1['snapshot']['storeName'],
          tracksCount: 3,
          tracksHash: '55b84a9d317184fe61224bfb4a060fb0'
        },
        delta: {
          withSnapshot: {
            id: 12345,
            takenAt: {
              timestamp: actualMetadata1i1['snapshot']['takenAt']['timestamp'],
              iso: actualMetadata1i1['snapshot']['takenAt']['iso']
            },
            storeName: actualMetadata1i1['snapshot']['storeName'],
            tracksCount: 3,
            tracksHash: '55b84a9d317184fe61224bfb4a060fb0'
          },
          addedTracksCount: 0,
          removedTracksCount: 0
        }
      };
      const expectedMetadata2 = {
        items: [expectedMetadataItem1, expectedMetadataItem2],
        total: 2,
        offset: 0,
        count: 2,
        limit: 999999999999
      };
      // check the general structure of the response
      const actualMetadata2 = await loadMetadata();
      expect(actualMetadata2).toMatchObject(expectedMetadata2);

      const actualMetadata2i2 = actualMetadata2['items'][1];
      // check that the date is consistent with the call
      expect(actualMetadata2i2['takenAt']['timestamp']).toBeGreaterThanOrEqual(beforeRun2.getTime());
      expect(actualMetadata2i2['takenAt']['timestamp']).toBeLessThanOrEqual(afterRun2.getTime());
      // check that the metadata date is different from the snapshot one
      expect(actualMetadata2i2['takenAt']['timestamp']).not.toStrictEqual(actualMetadata2i2['snapshot']['takenAt']['timestamp']);
      // check that the snapshot date is different from the delta one
      expect(actualMetadata2i2['takenAt']['timestamp']).not.toStrictEqual(actualMetadata2i2['delta']['withSnapshot']['takenAt']['timestamp']);

      // check that the metadata of run #1 has not changed
      expect(actualMetadata2['items'][0]).toStrictEqual(actualMetadata1['items'][0]);

      const actualData2 = await loadSnaphot(actualMetadata2i2);
      // check the snapshot data
      expect(actualData2).toStrictEqual({
        items: [track1, track2, track3],
        total: 3,
        offset: 0,
        count: 3,
        limit: 999999999999
      });

      // TODO: check that no email was sent
      // mocking 'apify' mocks all the Apify functions...
      // expect(Apify.call).not.toHaveBeenCalled();

      // ---------------------- run #3: tracks have been added and removed -----
      mockDeezerTracksRequest('run-03.json');
      const beforeRun3 = new Date();
      await playlistHistory.run();
      const afterRun3 = new Date();

      const expectedMetadataItem3 = {
        storeVersion: 1,
        takenAt: {
          timestamp: expect.any(Number),
          iso: expect.stringMatching(dateIsoRegEx)
        },
        snapshot: {
          id: 12345,
          takenAt: {
            timestamp: expect.any(Number),
            iso: expect.stringMatching(dateIsoRegEx)
          },
          storeName: expect.stringMatching(storeNameRegEx),
          tracksCount: 3,
          tracksHash: '9113b8baf90fd6674c6fe9cb922b48a0'
        },
        delta: {
          withSnapshot: {
            id: 12345,
            takenAt: {
              timestamp: actualMetadata2i2['snapshot']['takenAt']['timestamp'],
              iso: actualMetadata2i2['snapshot']['takenAt']['iso']
            },
            storeName: actualMetadata2i2['snapshot']['storeName'],
            tracksCount: 3,
            tracksHash: '55b84a9d317184fe61224bfb4a060fb0'
          },
          addedTracksCount: 1,
          removedTracksCount: 1
        }
      };
      const expectedMetadata3 = {
        items: [expectedMetadataItem1, expectedMetadataItem2, expectedMetadataItem3],
        total: 3,
        offset: 0,
        count: 3,
        limit: 999999999999
      };
      // check the general structure of the response
      const actualMetadata3 = await loadMetadata();
      expect(actualMetadata3).toMatchObject(expectedMetadata3);

      const actualMetadata3i3 = actualMetadata3['items'][2];
      // check that the date is consistent with the call
      expect(actualMetadata3i3['takenAt']['timestamp']).toBeGreaterThanOrEqual(beforeRun3.getTime());
      expect(actualMetadata3i3['takenAt']['timestamp']).toBeLessThanOrEqual(afterRun3.getTime());
      // check that the metadata date is the same than the snapshot one
      expect(actualMetadata3i3['takenAt']['timestamp']).toStrictEqual(actualMetadata3i3['snapshot']['takenAt']['timestamp']);
      // check that the snapshot date is different from the delta one
      expect(actualMetadata3i3['takenAt']['timestamp']).not.toStrictEqual(actualMetadata3i3['delta']['withSnapshot']['takenAt']['timestamp']);

      // check that the snapshot store is different from the delta one
      expect(actualMetadata3i3['snapshot']['storeName']).not.toStrictEqual(actualMetadata3i3['delta']['withSnapshot']['storeName']);

      // check that the metadata of run #1 and #2 have not changed
      expect(actualMetadata3['items'][0]).toStrictEqual(actualMetadata2['items'][0]);
      expect(actualMetadata3['items'][0]).toStrictEqual(actualMetadata1['items'][0]);
      expect(actualMetadata3['items'][1]).toStrictEqual(actualMetadata2['items'][1]);

      const actualData3 = await loadSnaphot(actualMetadata3i3);
      // check the snapshot data
      expect(actualData3).toStrictEqual({
        items: [track1, track2, track4],
        total: 3,
        offset: 0,
        count: 3,
        limit: 999999999999
      });

      // TODO: check that an email was sent
      // mocking 'apify' mocks all the Apify functions...
      // expect(Apify.call).toHaveBeenCalledWith();
    });

  });

  function mockDeezerTracksRequest(fileName) {
    nock('https://api.deezer.com')
      .get('/playlist/12345/tracks')
      .query({
        index: 0,
        limit: 9999
      })
      .replyWithFile(
        200,
        path.join(__dirname, 'PlaylistHistory', 'replies', fileName)
      );
  }

  async function loadMetadata() {
    const metadataStore = await Apify.openDataset('deezer-playlist-history--12345--metadata');
    return metadataStore.getData();
  }

  async function loadSnaphot(metadata) {
    const snapshotStoreName = metadata['snapshot']['storeName'];
    const snapshotStore = await Apify.openDataset(snapshotStoreName);
    return snapshotStore.getData();
  }

});
