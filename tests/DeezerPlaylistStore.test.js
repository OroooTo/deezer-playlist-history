const nock = require('nock');

const DeezerPlaylistStore = require('../src/DeezerPlaylistStore');

const tu = require('./testUtilities');

describe('DeezerPlaylist', () => {

  let deezerPlaylistStore;

  beforeEach(async () => {
    deezerPlaylistStore = new DeezerPlaylistStore(12345);
  });

  describe('when data has not been loaded', () => {

    test('#snapshot raise an error', async () => {
      expect(() => {
        deezerPlaylistStore.snapshot()
      }).toThrow('Data has not been loaded!');
    });

    test('#tracks raise an error', async () => {
      expect(() => {
        deezerPlaylistStore.tracks()
      }).toThrow('Data has not been loaded!');
    });

  });

  describe('when the playlist exists', () => {

    let dateBefore;
    let dateAfter;

    beforeEach(async () => {
      nock('https://api.deezer.com')
        .get('/playlist/12345/tracks')
        .query({
          index: 0,
          limit: 9999
        })
        .reply(200, {
          data: [
            tu.newDeezerTrack(1, 101, 201),
            tu.newDeezerTrack(2, 101, 202),
            tu.newDeezerTrack(3, 102, 203)
          ],
          checksum: '1234567890',
          total: 3
        });

      dateBefore = new Date();
      await deezerPlaylistStore.load();
      dateAfter = new Date();
    });

    test('#snapshot returns a snapshot of the playlist', async () => {
      const snapshot = deezerPlaylistStore.snapshot();
      expect(snapshot.takenAt.getTime()).toBeGreaterThanOrEqual(dateBefore.getTime());
      expect(snapshot.takenAt.getTime()).toBeLessThanOrEqual(dateAfter.getTime());
      expect(snapshot.tracksProperties).toStrictEqual([
        tu.newPlaylistTrackAsJSON(1, 101, 201),
        tu.newPlaylistTrackAsJSON(2, 101, 202),
        tu.newPlaylistTrackAsJSON(3, 102, 203)
      ]);
    });

    test('#tracks returns the tracks of the playlist', () => {
      const tracks = deezerPlaylistStore.tracks();
      expect(tracks).toStrictEqual([
        tu.newPlaylistTrack(1, 101, 201),
        tu.newPlaylistTrack(2, 101, 202),
        tu.newPlaylistTrack(3, 102, 203)
      ]);
    });

  });

  describe('when the playlist does not exist', () => {

    beforeEach(async () => {
      nock('https://api.deezer.com')
        .get('/playlist/12345/tracks')
        .query({
          index: 0,
          limit: 9999
        })
        .reply(200, {
          error: {
            type: 'DataException',
            message: 'no data',
            code: 800
          }
        });
    });

    test('#load raise an error', async () => {
      await expect(deezerPlaylistStore.load()).rejects.toThrow(new Error('Playlist 12345 does not exist!'));
    });

    test('#snapshot raise an error', async () => {
      expect(() => {
        deezerPlaylistStore.snapshot()
      }).toThrow('Data has not been loaded!');
    });

    test('#tracks raise an error', async () => {
      expect(() => {
        deezerPlaylistStore.tracks()
      }).toThrow('Data has not been loaded!');
    });

  });

});
