const Apify = require('apify');

const PlaylistSnapshot = require('../src/PlaylistSnapshot');
const PlaylistSnapshotStore = require('../src/PlaylistSnapshotStore');

const tu = require('./testUtilities');

describe('PlaylistSnapshotStore', () => {

  let playlistSnapshot;
  let playlistSnapshotStore;
  let playlistSnapshotStoreName;

  let playlistId;
  let playlistTracks;
  let playlistTracksAsJSON;
  let takenAt;
  let takenAtMs;

  beforeEach(async () => {
    playlistId = Math.floor(Math.random() * Math.floor(100000));
    playlistTracks = [
      tu.newPlaylistTrack(1, 101, 201),
      tu.newPlaylistTrack(2, 101, 202),
      tu.newPlaylistTrack(3, 102, 203)
    ];
    playlistTracksAsJSON = [
      tu.newPlaylistTrackAsJSON(1, 101, 201),
      tu.newPlaylistTrackAsJSON(2, 101, 202),
      tu.newPlaylistTrackAsJSON(3, 102, 203)
    ];
    takenAt = new Date(2000, 0, 1, 1, 2, 3, 4);
    takenAtMs = takenAt.getTime();

    playlistSnapshot = new PlaylistSnapshot(playlistId, playlistTracks, takenAt);

    playlistSnapshotStoreName = `deezer-playlist-history--${playlistId}--${takenAt.getTime()}`;
    playlistSnapshotStore = new PlaylistSnapshotStore(playlistId);
  });

  describe('when the store exist', () => {

    test('#load returns the snapshot of the playlist', async () => {
      const tracksStore = await Apify.openDataset(playlistSnapshotStoreName);
      await tracksStore.pushData(playlistTracks);
      const actualPlaylistSnapshot = await playlistSnapshotStore.load(takenAt);
      expect(actualPlaylistSnapshot).toStrictEqual(playlistSnapshot);
    });

    test('#save raise an error', async () => {
      // TODO
    });

  });

  describe('when the store does not exist', () => {

    test('#load raise an error', async () => {
      // Apify.openDataset creates a new dataset if it does not exist
      // TODO
      // await expect(playlistSnapshotStore.load(99999)).rejects.toThrow(new Error('Snapshot of the playlist 12345 at 99999 does not exist!'));
    });

    test('#save', async () => {
      await playlistSnapshotStore.save(playlistSnapshot);

      const tracksStore = await Apify.openDataset(playlistSnapshotStoreName);
      const actualTracksData = await tracksStore.getData();
      expect(actualTracksData).toStrictEqual({
        items: playlistTracksAsJSON,
        total: playlistTracksAsJSON.length,
        offset: 0,
        count: playlistTracksAsJSON.length,
        limit: 999999999999
      });
    });

  });

  describe('#storeName', () => {

    beforeEach(async () => {
      delete process.env.STORE_PREFIX;
    });

    test('return the default prefix when there is no custom one set', () => {
      expect(playlistSnapshotStore.storeName(takenAt)).toStrictEqual(`deezer-playlist-history--${playlistId}--${takenAtMs}`);
    });

    test('return the custom prefix when there is a custom one set', () => {
      process.env.STORE_PREFIX = 'another-store-prefix';
      expect(playlistSnapshotStore.storeName(takenAt)).toStrictEqual(`another-store-prefix--${playlistId}--${takenAtMs}`);
    });

  });

});
