const PlaylistMetadata = require('../src/PlaylistMetadata');
const PlaylistSnapshot = require('../src/PlaylistSnapshot');

const tu = require('./testUtilities');

describe('PlaylistMetadata', () => {

  let data;
  let playlistMetadata;
  let playlistSnapshot;

  beforeEach(() => {
    data = {
      storeVersion: 1,
      takenAt: {
        timestamp: 946782245006,
        iso: '2000-01-02T03:04:05.006Z'
      },
      snapshot: {
        id: 12345,
        takenAt: {
          timestamp: 946782245006,
          iso: '2000-01-02T03:04:05.006Z'
        },
        storeName: 'deezer-playlist-history--12345--987654321',
        tracksCount: 987,
        tracksHash: '0123456789abcdef0123456789abcdef'
      },
      delta: {
        withSnapshot: {
          id: 12345,
          takenAt: {
            timestamp: 946782245006,
            iso: '1999-01-02T03:04:05.006Z'
          },
          storeName: 'deezer-playlist-history--12345--915246245006',
          tracksCount: 654,
          tracksHash: 'abcdef0123456789abcdef0123456789'
        },
        addedTracksCount: 321,
        removedTracksCount: 123
      }
    };
    playlistMetadata = new PlaylistMetadata(data);

    const track1 = tu.newPlaylistTrack(1, 101, 201);
    const track2 = tu.newPlaylistTrack(2, 101, 201);
    const track3 = tu.newPlaylistTrack(3, 101, 202);
    const takenAt = new Date('2000-01-02T03:04:05.006Z');
    playlistSnapshot = new PlaylistSnapshot(12345, [track1, track2, track3], takenAt);
  });

  describe('constructor', () => {
    test('raises an error when the JSON is invalid (1)', () => {
      expect(() => {
        // eslint-disable-next-line no-new
        new PlaylistMetadata({});
      }).toThrow('Invalid data: ');
    });

    test('raises an error when the JSON is invalid (2)', () => {
      data['snapshot'] = null;
      expect(() => {
        // eslint-disable-next-line no-new
        new PlaylistMetadata(data);
      }).toThrow('Invalid data: ');
    });
  });

  describe('#fromJSON', () => {
    test('creates a new PlaylistMetadata', () => {
      expect(PlaylistMetadata.fromJSON(data).toJSON()).toStrictEqual(data);
    });

    test('raises an error when the JSON is invalid (1)', () => {
      expect(() => {
        PlaylistMetadata.fromJSON({});
      }).toThrow('Invalid data: ');
    });

    test('raises an error when the JSON is invalid (2)', () => {
      data['snapshot'] = null;
      expect(() => {
        PlaylistMetadata.fromJSON(data);
      }).toThrow('Invalid data: ');
    });
  });

  describe('#toJSON', () => {
    test('returns the underlying JSON', () => {
      expect(playlistMetadata.toJSON()).toStrictEqual(data);
    });
  });

  describe('#snapshotTracksHash', () => {
    test('returns the track hash of the snapshot', () => {
      expect(playlistMetadata.snapshotTracksHash).toStrictEqual('0123456789abcdef0123456789abcdef');
    });
  });

  describe('#snapshotStoreName', () => {
    test('returns the store name of the snapshot', () => {
      expect(playlistMetadata.snapshotStoreName).toStrictEqual('deezer-playlist-history--12345--987654321');
    });
  });

  describe('#snapshotStoreTakenAt', () => {
    test('returns the store date of the snapshot', () => {
      expect(playlistMetadata.snapshotStoreTakenAt).toStrictEqual(new Date('2000-01-02T03:04:05.006Z'));
    });
  });

  describe('#takenAt', () => {
    test('returns the date at which the metadata was taken', () => {
      expect(playlistMetadata.takenAt).toStrictEqual(new Date(946782245006));
    });
  });

  describe('#fromSnapshot', () => {
    test('returns an initial PlaylistMetadata', () => {
      expect(PlaylistMetadata.fromSnapshot(playlistSnapshot).toJSON()).toStrictEqual({
        storeVersion: 1,
        takenAt: {
          timestamp: 946782245006,
          iso: '2000-01-02T03:04:05.006Z'
        },
        snapshot: {
          id: 12345,
          takenAt: {
            timestamp: 946782245006,
            iso: '2000-01-02T03:04:05.006Z'
          },
          storeName: 'deezer-playlist-history--12345--946782245006',
          tracksCount: 3,
          tracksHash: '55b84a9d317184fe61224bfb4a060fb0'
        },
        delta: {
          withSnapshot: null,
          addedTracksCount: 3,
          removedTracksCount: 0
        }
      });
    });
  });

  describe('#noChangeFrom', () => {
    test('returns an updated PlaylistMetadata', () => {
      // TODO
    });
  });

  describe('#deltaBetween', () => {
    test('returns an updated PlaylistMetadata', () => {
      // TODO
    });
  });

});
