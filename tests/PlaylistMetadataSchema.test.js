const PlaylistMetadataSchema = require('../src/PlaylistMetadataSchema');

describe('PlaylistMetadataSchema', () => {

  let playlistMetadataSchema;
  let data;

  beforeEach(() => {
    playlistMetadataSchema = new PlaylistMetadataSchema();
    data = {
      storeVersion: 1,
      takenAt: {
        timestamp: 946782245006,
        iso: '2000-01-02T03:04:05.006Z'
      },
      snapshot: {
        id: 12345,
        takenAt: {
          timestamp: 946782245006,
          iso: '2000-01-02T03:04:05.006Z'
        },
        storeName: 'deezer-playlist-history--12345--987654321',
        tracksCount: 987,
        tracksHash: '0123456789abcdef0123456789abcdef'
      },
      delta: {
        withSnapshot: {
          id: 12345,
          takenAt: {
            timestamp: 946782245006,
            iso: '1999-01-02T03:04:05.006Z'
          },
          storeName: 'deezer-playlist-history--12345--915246245006',
          tracksCount: 654,
          tracksHash: 'abcdef0123456789abcdef0123456789'
        },
        addedTracksCount: 321,
        removedTracksCount: 123
      }
    };
  });

  describe('#validate', () => {

    describe('when data is valid', () => {

      test('valid data', () => {
        playlistMetadataSchema.validate(data);
      });

      test('valid data without delta.withSnapshot', () => {
        data['delta']['withSnapshot'] = null;
        playlistMetadataSchema.validate(data);
      });

    });

    describe('when data is invalid', () => {

      test('invalid data: storeVersion', () => {
        data['storeVersion'] = 'integer';
        expect(() => {
          playlistMetadataSchema.validate(data);
        }).toThrow('Invalid data: data/storeVersion must be integer');
      });

      test('invalid data: takenAt', () => {
        data['takenAt'] = null;
        expect(() => {
          playlistMetadataSchema.validate(data);
        }).toThrow('Invalid data: data/takenAt must be object');
      });

      test('invalid data: takenAt.timestamp', () => {
        data['takenAt']['timestamp'] = 'integer';
        expect(() => {
          playlistMetadataSchema.validate(data);
        }).toThrow('Invalid data: data/takenAt/timestamp must be integer');
      });

      test('invalid data: takenAt.iso', () => {
        data['takenAt']['iso'] = '2000/01/02 03h04m05s006ms';
        expect(() => {
          playlistMetadataSchema.validate(data);
        }).toThrow('Invalid data: data/takenAt/iso must match pattern "^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}Z$"');
      });

      test('invalid data: snapshot', () => {
        data['snapshot'] = null;
        expect(() => {
          playlistMetadataSchema.validate(data);
        }).toThrow('Invalid data: data/snapshot must be object');
      });

      test('invalid data: snapshot.storeName', () => {
        data['snapshot']['storeName'] = 'my-store';
        expect(() => {
          playlistMetadataSchema.validate(data);
        }).toThrow('Invalid data: data/snapshot/storeName must match pattern "^.+--[0-9]+--[0-9]+$"');
      });

      test('invalid data: snapshot.tracksCount', () => {
        data['snapshot']['tracksCount'] = 'integer';
        expect(() => {
          playlistMetadataSchema.validate(data);
        }).toThrow('Invalid data: data/snapshot/tracksCount must be integer');
      });

      test('invalid data: snapshot.tracksHash', () => {
        data['snapshot']['tracksHash'] = '1234567890 xyz';
        expect(() => {
          playlistMetadataSchema.validate(data);
        }).toThrow('Invalid data: data/snapshot/tracksHash must match pattern "^[0-9a-f]{32}$"');
      });

      test('invalid data: delta', () => {
        data['delta'] = null;
        expect(() => {
          playlistMetadataSchema.validate(data);
        }).toThrow('Invalid data: data/delta must be object');
      });

      test('invalid data: delta.addedTracksCount', () => {
        data['delta']['addedTracksCount'] = 'integer';
        expect(() => {
          playlistMetadataSchema.validate(data);
        }).toThrow('Invalid data: data/delta/addedTracksCount must be integer');
      });

      test('invalid data: delta.removedTracksCount', () => {
        data['delta']['removedTracksCount'] = 'integer';
        expect(() => {
          playlistMetadataSchema.validate(data);
        }).toThrow('Invalid data: data/delta/removedTracksCount must be integer');
      });

      // delta.withSnapshot.* is the same object than snapshot

    });

  });

});
