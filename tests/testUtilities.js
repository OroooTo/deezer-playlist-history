const crypto = require('crypto');

// -------------------------------------------------------------------------------------------------------- Deezer -----

function newDeezerTrack(trackId, artistId, albumId) {
  return {
    id: trackId,
    readable: true,
    title: `Track ${trackId}`,
    title_short: `Track ${trackId}`,
    title_version: '',
    link: `https://www.deezer.com/track/${trackId}`,
    duration: trackId * 10,
    rank: trackId * 100,
    explicit_lyrics: false,
    explicit_content_lyrics: 0,
    explicit_content_cover: 0,
    preview: `https://cdns-preview.dzcdn.net/stream/${trackId}.mp3`,
    time_add: 1234567890,
    artist: {
      id: artistId,
      name: `Artist ${artistId}`,
      link: `https://www.deezer.com/artist/${artistId}`,
      picture: `https://api.deezer.com/artist//${artistId}/image`,
      picture_small: `https://cdns-images.dzcdn.net/images/${artistId}/small.jpg`,
      picture_medium: `https://cdns-images.dzcdn.net/images/${artistId}/medium.jpg`,
      picture_big: `https://cdns-images.dzcdn.net/images/${artistId}/big.jpg`,
      picture_xl: `https://cdns-images.dzcdn.net/images/${artistId}/xl.jpg`,
      tracklist: `https://api.deezer.com/artist/${artistId}`,
      type: 'artist'
    },
    album: {
      id: albumId,
      title: `Album ${albumId}`,
      cover: `https://api.deezer.com/album/${albumId}/image`,
      cover_small: `https://cdns-images.dzcdn.net/images/cover/${albumId}/small.jpg`,
      cover_medium: `https://cdns-images.dzcdn.net/images/cover/${albumId}/medium.jpg`,
      cover_big: `https://cdns-images.dzcdn.net/images/cover/${albumId}/big.jpg`,
      cover_xl: `https://cdns-images.dzcdn.net/images/cover/${albumId}/xl.jpg`,
      tracklist: `https://api.deezer.com/album/${albumId}/tracks`,
      type: 'album'
    },
    type: 'track'
  };
}

exports.newDeezerTrack = newDeezerTrack;

// ---------------------------------------------------------------------------------------------------------------------

const PlaylistTrack = require('../src/PlaylistTrack');

function newPlaylistMetadataAsJSON(tracksIds, addedCount, removedCount) {
  return {
    storeVersion: 1,
    takenAt: {
      timestamp: 946782245006,
      iso: '2000-01-02T03:04:05.006Z'
    },
    snapshot: {
      id: 12345,
      takenAt: {
        timestamp: 946782245006,
        iso: '2000-01-02T03:04:05.006Z'
      },
      storeName: 'deezer-playlist-history--12345--946782245006',
      tracksCount: tracksIds.length,
      tracksHash: crypto.createHash('md5').update(tracksIds.join(',')).digest('hex')
    },
    delta: {
      withSnapshot: {
        id: 12345,
        takenAt: {
          timestamp: 946782245006,
          iso: '1999-01-02T03:04:05.006Z'
        },
        storeName: 'deezer-playlist-history--12345--915246245006',
        tracksCount: 654,
        tracksHash: 'abcdef0123456789abcdef0123456789'
      },
      addedTracksCount: addedCount,
      removedTracksCount: removedCount
    }
  }
}

function newPlaylistTrack(trackId, artistId, albumId) {
  return new PlaylistTrack(trackId, `Track ${trackId}`, artistId, `Artist ${artistId}`, albumId, `Album ${albumId}`);
}

function newPlaylistTrackAsJSON(trackId, artistId, albumId) {
  return {
    track_id: trackId,
    track_title: `Track ${trackId}`,
    artist_id: artistId,
    artist_name: `Artist ${artistId}`,
    album_id: albumId,
    album_title: `Album ${albumId}`
  };
}

exports.newPlaylistMetadataAsJSON = newPlaylistMetadataAsJSON;
exports.newPlaylistTrack = newPlaylistTrack;
exports.newPlaylistTrackAsJSON = newPlaylistTrackAsJSON;
