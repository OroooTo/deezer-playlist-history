const PlaylistTrack = require('../src/PlaylistTrack');

const tu = require('./testUtilities');

describe('PlaylistTrack', () => {

  let track = null;

  beforeEach(() => {
    track = tu.newPlaylistTrack(1, 101, 201);
  });

  test('#trackId', () => {
    expect(track.trackId).toStrictEqual(1);
  });

  test('#trackTitle', () => {
    expect(track.trackTitle).toStrictEqual('Track 1');
  });

  test('#artistId', () => {
    expect(track.artistId).toStrictEqual(101);
  });

  test('#artistName', () => {
    expect(track.artistName).toStrictEqual('Artist 101');
  });

  test('#albumId', () => {
    expect(track.albumId).toStrictEqual(201);
  });

  test('#albumTitle', () => {
    expect(track.albumTitle).toStrictEqual('Album 201');
  });

  test('#toJSON', () => {
    expect(track.toJSON())
      .toStrictEqual(tu.newPlaylistTrackAsJSON(1, 101, 201));
  });

  test('#fromJSON', () => {
    expect(PlaylistTrack.fromJSON(tu.newPlaylistTrackAsJSON(1, 101, 201)))
      .toStrictEqual(tu.newPlaylistTrack(1, 101, 201));
  });

});
