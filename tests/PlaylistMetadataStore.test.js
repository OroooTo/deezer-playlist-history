const Apify = require('apify');
const tmp = require('tmp');

const PlaylistMetadata = require('../src/PlaylistMetadata');
const PlaylistMetadataStore = require('../src/PlaylistMetadataStore');

const tu = require('./testUtilities');

describe('PlaylistMetadataStore', () => {

  let metadataStoreName;
  let playlistId;
  let playlistMetadataStore;

  beforeEach(() => {
    playlistId = Math.floor(Math.random() * Math.floor(100000));
    metadataStoreName = `deezer-playlist-history--${playlistId}--metadata`;
    playlistMetadataStore = new PlaylistMetadataStore(playlistId);
  });

  describe('when data has not been loaded', () => {

    test('#entries', async () => {
      expect(() => {
        playlistMetadataStore.entries()
      }).toThrow('Data has not been loaded!');
    });

    test('#entriesCount', async () => {
      expect(() => {
        playlistMetadataStore.entriesCount()
      }).toThrow('Data has not been loaded!');
    });

    test('#hasEntry', async () => {
      expect(() => {
        playlistMetadataStore.hasEntry()
      }).toThrow('Data has not been loaded!');
    });

    test('#lastEntry', async () => {
      expect(() => {
        playlistMetadataStore.lastEntry()
      }).toThrow('Data has not been loaded!');
    });

    test('#save', async () => {
      // TODO
    });

  });

  describe('when the store is empty', () => {

    beforeEach(async () => {
      await playlistMetadataStore.load();
    });

    test('#entries', async () => {
      expect(playlistMetadataStore.entries()).toStrictEqual([]);
    });

    test('#entriesCount', async () => {
      expect(playlistMetadataStore.entriesCount()).toBe(0);
    });

    test('#hasEntry', async () => {
      expect(playlistMetadataStore.hasEntry()).toBeFalsy();
    });

    test('#lastEntry', async () => {
      expect(playlistMetadataStore.lastEntry()).toBeNull();
    });

    test('#save', async () => {
      const metadata = tu.newPlaylistMetadataAsJSON([1, 2, 3], 0, 0);
      await playlistMetadataStore.save(new PlaylistMetadata(metadata));
      await playlistMetadataStore.load();
      expect(playlistMetadataStore.entries()).toStrictEqual([
        new PlaylistMetadata(metadata)
      ]);
    });

  });

  describe('when the store has 2 entries', () => {
    const metadata1 = tu.newPlaylistMetadataAsJSON([1, 2, 3], 0, 0);
    const metadata2 = tu.newPlaylistMetadataAsJSON([1, 2, 3, 4], 1, 0);

    beforeEach(async () => {
      const metadataStore = await Apify.openDataset(metadataStoreName);
      await metadataStore.pushData(metadata1);
      await metadataStore.pushData(metadata2);
      await playlistMetadataStore.load();
    });

    test('#entries', async () => {
      expect(playlistMetadataStore.entries()).toStrictEqual([
        new PlaylistMetadata(metadata1),
        new PlaylistMetadata(metadata2)
      ]);
    });

    test('#entriesCount', async () => {
      expect(playlistMetadataStore.entriesCount()).toBe(2);
    });

    test('#hasEntry', async () => {
      expect(playlistMetadataStore.hasEntry()).toBeTruthy();
    });

    test('#lastEntry', async () => {
      expect(playlistMetadataStore.lastEntry()).toStrictEqual(new PlaylistMetadata(metadata2));
    });

    test('#save', async () => {
      const metadata3 = tu.newPlaylistMetadataAsJSON([1, 2, 3], 0, 0);
      await playlistMetadataStore.save(new PlaylistMetadata(metadata3));
      await playlistMetadataStore.load();
      expect(playlistMetadataStore.entries()).toStrictEqual([
        new PlaylistMetadata(metadata1),
        new PlaylistMetadata(metadata2),
        new PlaylistMetadata(metadata3)
      ]);
    });

  });

  describe('#storeName', () => {

    beforeEach(async () => {
      delete process.env.STORE_PREFIX;
    });

    test('return the default prefix when there is no custom one set', () => {
      expect(playlistMetadataStore.storeName()).toStrictEqual(`deezer-playlist-history--${playlistId}--metadata`);
    });

    test('return the custom prefix when there is a custom one set', () => {
      process.env.STORE_PREFIX = 'another-store-prefix';
      expect(playlistMetadataStore.storeName()).toStrictEqual(`another-store-prefix--${playlistId}--metadata`);
    });

  });

});
