const Apify = require('apify');
const PlaylistHistory = require('./PlaylistHistory');
const logger = require('./logger');

Apify.main(async () => {
  const input = await Apify.getInput();
  logger.debug("Input variable 'playlistId' = %s", input['playlistId']);
  logger.debug("Input variable 'email' = %s", input['email']);
  const playlistHistory = new PlaylistHistory(input['playlistId'], input['email']);
  await playlistHistory.run();
});
