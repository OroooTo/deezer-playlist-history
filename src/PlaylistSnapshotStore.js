const apify = require('apify');

const logger = require('./logger');
const PlaylistHistorySettings = require('./PlaylistHistorySettings');
const PlaylistSnapshot = require('./PlaylistSnapshot');
const PlaylistTrack = require('./PlaylistTrack');

class PlaylistSnapshotStore {

  /**
   * @param playlistId {int}
   */
  constructor(playlistId) {
    this._playlistId = playlistId;
  }

  /**
   * @returns {int}
   */
  get playlistId() {
    return this._playlistId;
  }

  /**
   * @param takenAt {Date}
   * @returns {Promise<PlaylistSnapshot>}
   */
  async load(takenAt) {
    const tracksStoreName = this.storeName(takenAt);
    logger.debug("Loading snapshot from '%s'", tracksStoreName);
    const tracksStore = await apify.openDataset(tracksStoreName);
    const tracksProperties = await tracksStore.getData();
    const tracks = tracksProperties['items'].map(trackAsJSON => PlaylistTrack.fromJSON(trackAsJSON));
    logger.debug("Loaded snapshot from '%s' with %d tracks", tracksStoreName, tracks.length);
    return new PlaylistSnapshot(this.playlistId, tracks, takenAt);
  }

  /**
   * @param playlistSnapshot {PlaylistSnapshot}
   * @returns {Promise<void>}
   */
  async save(playlistSnapshot) {
    const tracksStoreName = this.storeName(playlistSnapshot.takenAt);
    logger.debug("Saving snapshot into '%s'", tracksStoreName);
    const tracksStore = await apify.openDataset(tracksStoreName);
    await tracksStore.pushData(playlistSnapshot.tracksProperties);
    logger.debug("Saved snapshot into '%s'", tracksStoreName);
  }

  /**
   * @param takenAt {Date}
   * @returns {string}
   */
  storeName(takenAt) {
    return `${PlaylistHistorySettings.STORE_PREFIX}--${this.playlistId}--${takenAt.getTime()}`;
  }

}

module.exports = PlaylistSnapshotStore;
