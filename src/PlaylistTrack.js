class PlaylistTrack {

  /**
   * @param trackId {int}
   * @param trackTitle {string}
   * @param artistId {int}
   * @param artistName {string}
   * @param albumId {int}
   * @param albumTitle {string}
   */
  constructor(trackId, trackTitle, artistId, artistName, albumId, albumTitle) {
    this._trackId = trackId;
    this._trackTitle = trackTitle;
    this._artistId = artistId;
    this._artistName = artistName;
    this._albumId = albumId;
    this._albumTitle = albumTitle;
  }

  /**
   * @returns {int}
   */
  get trackId() {
    return this._trackId;
  }

  /**
   * @returns {string}
   */
  get trackTitle() {
    return this._trackTitle;
  }

  /**
   * @returns {int}
   */
  get artistId() {
    return this._artistId;
  }

  /**
   * @returns {string}
   */
  get artistName() {
    return this._artistName;
  }

  /**
   * @returns {int}
   */
  get albumId() {
    return this._albumId;
  }

  /**
   * @returns {string}
   */
  get albumTitle() {
    return this._albumTitle;
  }

  /**
   * @returns {{artist_name: string, track_id: int, track_title: string, album_id: int, artist_id: int, album_title: string}}
   */
  toJSON() {
    return {
      track_id: this.trackId,
      track_title: this.trackTitle,
      artist_id: this.artistId,
      artist_name: this.artistName,
      album_id: this.albumId,
      album_title: this.albumTitle
    };
  }

  /**
   * @param trackAsJSON {{artist_name: string, track_id: int, track_title: string, album_id: int, artist_id: int, album_title: string}}
   * @returns {PlaylistTrack}
   */
  static fromJSON(trackAsJSON) {
    // TODO check JSON schema
    return new PlaylistTrack(
      trackAsJSON['track_id'], trackAsJSON['track_title'],
      trackAsJSON['artist_id'], trackAsJSON['artist_name'],
      trackAsJSON['album_id'], trackAsJSON['album_title']);
  }

}

module.exports = PlaylistTrack;
