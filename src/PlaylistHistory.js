const logger = require('./logger');
const DeezerPlaylistStore = require('./DeezerPlaylistStore');
const PlaylistMailer = require('../src/PlaylistMailer');
const PlaylistMetadata = require('./PlaylistMetadata');
const PlaylistMetadataStore = require('./PlaylistMetadataStore');
const PlaylistSnapshotStore = require('./PlaylistSnapshotStore');

class PlaylistHistory {

  /**
   * @param playlistId {int}
   * @param email {string|null}
   */
  constructor(playlistId, email = null) {
    this._playlistId = playlistId;
    this._email = email;
  }

  /**
   * @returns {int}
   */
  get playlistId() {
    return this._playlistId;
  }

  /**
   * @returns {string|null}
   */
  get email() {
    return this._email;
  }

  async run() {
    logger.info('Running PlaylistHistory for playlist %s', this.playlistId);

    // fetch the tracks of the given Deezer playlist
    const deezerPlaylistStore = new DeezerPlaylistStore(this.playlistId);
    await deezerPlaylistStore.load();
    const currentPlaylistSnapshot = deezerPlaylistStore.snapshot();

    // fetch the last metadata for the given playlist
    const playlistMetadataStore = new PlaylistMetadataStore(this.playlistId);
    await playlistMetadataStore.load();

    if (playlistMetadataStore.hasEntry()) {
      logger.info('This is not the first run for the playlist %d', this.playlistId);

      // fetch the metadata of the last run
      const previousPlaylistMetadata = playlistMetadataStore.lastEntry();
      logger.debug('Previous metadata: %j', previousPlaylistMetadata);

      // initialize the metadata for this run
      const currentPlaylistMetadata = PlaylistMetadata.fromSnapshot(currentPlaylistSnapshot);

      if (previousPlaylistMetadata.snapshotTracksHash === currentPlaylistMetadata.snapshotTracksHash) {
        logger.info('Tracks have not been modified');

        // do not save the current snapshot, use the previous one

        // save updated metadata
        currentPlaylistMetadata.noChangeFrom(previousPlaylistMetadata);
        await playlistMetadataStore.save(currentPlaylistMetadata);

      } else {
        logger.info('Tracks have been modified');

        // save the current snapshot
        const playlistSnapshotStore = new PlaylistSnapshotStore(this.playlistId);
        await playlistSnapshotStore.save(currentPlaylistSnapshot);

        // load previous snapshot (required for tracks IDs)
        const previousPlaylistSnapshot = await playlistSnapshotStore.load(previousPlaylistMetadata.snapshotStoreTakenAt);

        // compute delta between snapshots and save metadata
        currentPlaylistMetadata.deltaBetween(previousPlaylistMetadata, currentPlaylistSnapshot, previousPlaylistSnapshot);
        await playlistMetadataStore.save(currentPlaylistMetadata);

        if (this.email) {
          logger.info("Sending an email to '%s'", this.email);
          await new PlaylistMailer(this.email).snapshotChanged(previousPlaylistSnapshot, currentPlaylistSnapshot);
        } else {
          logger.info('Not sending an email because no email was specified');
        }
      }

    } else {
      logger.info('This is the first run for the playlist %d', this.playlistId);

      // save the snapshot
      const playlistSnapshotStore = new PlaylistSnapshotStore(this.playlistId);
      await playlistSnapshotStore.save(currentPlaylistSnapshot);

      // save the  associated metadata
      const playlistMetadata = PlaylistMetadata.fromSnapshot(currentPlaylistSnapshot);
      await playlistMetadataStore.save(playlistMetadata);

      // not sending an email
    }
  }

}

module.exports = PlaylistHistory;
