const PlaylistHistorySettings = require('./PlaylistHistorySettings');
const PlaylistMetadataSchema = require('./PlaylistMetadataSchema');

class PlaylistMetadata {

  /**
   * PRIVATE! DO NOT USE DIRECTLY!
   * @param metadataAsJSON {object}
   * @private
   */
  constructor(metadataAsJSON) {
    new PlaylistMetadataSchema().validate(metadataAsJSON);
    this._metadataAsJSON = metadataAsJSON;
  }

  /**
   * @returns {int}
   */
  get snapshotId() {
    return this._metadataAsJSON['snapshot']['id'];
  }

  /**
   * @returns {String}
   */
  get snapshotTracksHash() {
    return this._metadataAsJSON['snapshot']['tracksHash'];
  }

  /**
   * @returns {String}
   */
  get snapshotStoreName() {
    return this._metadataAsJSON['snapshot']['storeName'];
  }

  /**
   * @returns {Date}
   */
  get snapshotStoreTakenAt() {
    return new Date(this._metadataAsJSON['snapshot']['takenAt']['timestamp']);
  }

  /**
   * @returns {Date}
   */
  get takenAt() {
    return new Date(this._metadataAsJSON['takenAt']['timestamp']);
  }

  /**
   * @returns {String}
   */
  toJSON() {
    return this._metadataAsJSON;
  }

  /**
   * @param metadataAsJSON {Object}
   * @returns {PlaylistMetadata}
   */
  static fromJSON(metadataAsJSON) {
    return new PlaylistMetadata(metadataAsJSON);
  }

  /**
   * @param playlistSnapshot {PlaylistSnapshot}
   * @returns {PlaylistMetadata}
   */
  static fromSnapshot(playlistSnapshot) {
    return new PlaylistMetadata({
      storeVersion: PlaylistHistorySettings.STORE_VERSION,
      takenAt: {
        timestamp: playlistSnapshot.takenAt.getTime(),
        iso: playlistSnapshot.takenAt.toISOString()
      },
      snapshot: PlaylistMetadata._playlistSnapshotToJSON(playlistSnapshot),
      delta: {
        withSnapshot: null,
        addedTracksCount: playlistSnapshot.tracksIds.length,
        removedTracksCount: 0
      }
    });
  }

  /**
   * @param previousPlaylistMetadata {PlaylistMetadata}
   */
  noChangeFrom(previousPlaylistMetadata) {
    if (this.snapshotId !== previousPlaylistMetadata.snapshotId) {
      throw new Error('Snapshot ID must be the same!');
    }
    const previousPlaylistMetadataAsJSON = previousPlaylistMetadata.toJSON()['snapshot'];
    this._metadataAsJSON['snapshot'] = previousPlaylistMetadataAsJSON;
    this._metadataAsJSON['delta']['withSnapshot'] = previousPlaylistMetadataAsJSON;
    this._metadataAsJSON['delta']['addedTracksCount'] = 0;
    this._metadataAsJSON['delta']['removedTracksCount'] = 0;
  }

  /**
   * @param previousPlaylistMetadata {PlaylistMetadata}
   * @param currentPlaylistSnapshot {PlaylistSnapshot}
   * @param previousPlaylistSnapshot {PlaylistSnapshot}
   */
  deltaBetween(previousPlaylistMetadata, currentPlaylistSnapshot, previousPlaylistSnapshot) {
    if (this.snapshotId !== previousPlaylistMetadata.snapshotId) {
      throw new Error('Snapshot ID must be the same!');
    }
    this._metadataAsJSON['delta']['withSnapshot'] = previousPlaylistMetadata.toJSON()['snapshot'];
    this._metadataAsJSON['delta']['addedTracksCount'] = PlaylistMetadata._addedElements(previousPlaylistSnapshot.tracksIds, currentPlaylistSnapshot.tracksIds).length;
    this._metadataAsJSON['delta']['removedTracksCount'] = PlaylistMetadata._removedElements(previousPlaylistSnapshot.tracksIds, currentPlaylistSnapshot.tracksIds).length;
  }

  /**
   * @param playlistId {int}
   * @param takenAt {Date}
   * @returns {string}
   */
  static storeName(playlistId, takenAt) {
    return `${PlaylistHistorySettings.STORE_PREFIX}--${playlistId}--${takenAt.getTime()}`;
  }

  // ---------------------------

  /**
   * @param array1 {object[]}
   * @param array2 {object[]}
   * @returns {object[]}
   * @private
   */
  static _difference(array1, array2) {
    return array1.filter(x => !array2.includes(x));
  }

  /**
   * @param previous {object[]}
   * @param current {object[]}
   * @returns {object[]}
   * @private
   */
  static _addedElements(previous, current) {
    return PlaylistMetadata._difference(current, previous);
  }

  /**
   * @param previous {object[]}
   * @param current {object[]}
   * @returns {object[]}
   * @private
   */
  static _removedElements(previous, current) {
    return PlaylistMetadata._difference(previous, current);
  }

  /**
   * @param playlistSnapshot {PlaylistSnapshot}
   * @returns {{tracksCount: number, takenAt: {iso: string, timestamp: number}, storeName: string, id: int, tracksHash: (*|string)}}
   * @private
   */
  static _playlistSnapshotToJSON(playlistSnapshot) {
    return {
      id: playlistSnapshot.playlistId,
      takenAt: {
        timestamp: playlistSnapshot.takenAt.getTime(),
        iso: playlistSnapshot.takenAt.toISOString()
      },
      storeName: PlaylistMetadata.storeName(playlistSnapshot.playlistId, playlistSnapshot.takenAt),
      tracksCount: playlistSnapshot.tracksIds.length,
      tracksHash: playlistSnapshot.tracksHash
    };
  }

}

module.exports = PlaylistMetadata;
