const crypto = require('crypto');

class PlaylistSnapshot {

  /**
   * @param playlistId {int}
   * @param playlistTracks {PlaylistTrack[]}
   * @param takenAt {Date}
   */
  constructor(playlistId, playlistTracks, takenAt) {
    this._playlistId = playlistId;
    this._playlistTracks = playlistTracks;
    this._takenAt = takenAt;
    this._tracksProperties = PlaylistSnapshot._toTracksProperties(this._playlistTracks);
    this._tracksIds = PlaylistSnapshot._toTracksIds(this._tracksProperties);
    this._tracksHash = PlaylistSnapshot._toTracksHash(this._tracksIds);
  }

  /**
   * @returns {int}
   */
  get playlistId() {
    return this._playlistId;
  }

  /**
   * @returns {PlaylistTrack[]}
   */
  get playlistTracks() {
    return this._playlistTracks;
  }

  /**
   * @returns {Date}
   */
  get takenAt() {
    return this._takenAt;
  }

  /**
   * @returns {{artist_name: string, track_id: int, track_title: string, album_id: int, artist_id: int, album_title: string}[]}
   */
  get tracksProperties() {
    return this._tracksProperties;
  }

  /**
   * @returns {int[]}
   */
  get tracksIds() {
    return this._tracksIds;
  }

  /**
   * @returns {string}
   */
  get tracksHash() {
    return this._tracksHash;
  }

  /**
   * @param tracks {PlaylistTrack[]}
   * @returns {{artist_name: string, track_id: int, track_title: string, album_id: int, artist_id: int, album_title: string}[]}
   * @private
   */
  static _toTracksProperties(tracks) {
    return tracks.map(
      function (track) {
        return track.toJSON();
      }
    );
  }

  /**
   * @param tracksProperties {{artist_name: string, track_id: int, track_title: string, album_id: int, artist_id: int, album_title: string}[]}
   * @returns {int[]}
   * @private
   */
  static _toTracksIds(tracksProperties) {
    return tracksProperties.map(track => track['track_id']).sort();
  }

  /**
   * @param tracksIds {int[]}
   * @returns {string}
   * @private
   */
  static _toTracksHash(tracksIds) {
    return crypto.createHash('md5').update(tracksIds.join(',')).digest('hex');
  }

}

module.exports = PlaylistSnapshot;
