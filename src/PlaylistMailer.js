const Apify = require('apify');

const logger = require('./logger');

class PlaylistMailer {

  /**
   * @param email {String}
   */
  constructor(email) {
    this._email = email;
  }

  /**
   * @returns {String}
   */
  get email() {
    return this._email;
  }

  async snapshotChanged(previousPlaylistSnapshot, currentPlaylistSnapshot) {
    logger.debug("Snapshot changed between snapshots taken at '%s' and '%s'", previousPlaylistSnapshot.takenAt, currentPlaylistSnapshot.takenAt);

    if (previousPlaylistSnapshot.playlistId !== currentPlaylistSnapshot.playlistId) {
      throw new Error(`Playlists must be the same but are #${previousPlaylistSnapshot.playlistId} and #${currentPlaylistSnapshot.playlistId}`);
    }

    const addedTracks = PlaylistMailer._addedTracks(previousPlaylistSnapshot.playlistTracks, currentPlaylistSnapshot.playlistTracks);
    const removedTracks = PlaylistMailer._removedTracks(previousPlaylistSnapshot.playlistTracks, currentPlaylistSnapshot.playlistTracks);

    const content = `
<p>
Your Deezer playlist #${currentPlaylistSnapshot.playlistId} has changed<br>
between ${previousPlaylistSnapshot.takenAt.toISOString()}<br>
and ${currentPlaylistSnapshot.takenAt.toISOString()}<br>
</p>
<h2>Added tracks</h2>
${PlaylistMailer._toHtmlTable(addedTracks)}
<h2>Removed tracks</h2>
${PlaylistMailer._toHtmlTable(removedTracks)}`;

    logger.info("Sending an email to '%s' (%s tracks added, %d tracks removed)", this.email, addedTracks.length, removedTracks.length);
    await Apify.call('apify/send-mail', {
      to: this._email,
      subject: `Your Deezer playlist #${currentPlaylistSnapshot.playlistId} has changed`,
      html: content
    });
  }

  // ---------------------------

  /**
   * @param objects1 {Object[]}
   * @param objects2 {Object[]}
   * @returns {Object[]}
   * @private
   */
  static _difference(objects1, objects2) {
    return objects1.filter(x => !objects2.includes(x));
  }

  /**
   * @param previousPlaylistTracks {PlaylistTrack[]}
   * @param currentPlaylistTracks {PlaylistTrack[]}
   * @returns {PlaylistTrack[]}
   * @private
   */
  static _addedTracks(previousPlaylistTracks, currentPlaylistTracks) {
    const addedTracksIds = PlaylistMailer._difference(
      currentPlaylistTracks.map(t => t.trackId),
      previousPlaylistTracks.map(t => t.trackId));
    return currentPlaylistTracks.filter(t => addedTracksIds.includes(t.trackId));
  }

  /**
   * @param previousPlaylistTracks {PlaylistTrack[]}
   * @param currentPlaylistTracks {PlaylistTrack[]}
   * @returns {PlaylistTrack[]}
   * @private
   */
  static _removedTracks(previousPlaylistTracks, currentPlaylistTracks) {
    const removedTracksIds = PlaylistMailer._difference(
      previousPlaylistTracks.map(t => t.trackId),
      currentPlaylistTracks.map(t => t.trackId));
    return previousPlaylistTracks.filter(t => removedTracksIds.includes(t.trackId));
  }

  /**
   * @param playlistTracks {PlaylistTrack[]}
   * @returns {string}
   * @private
   */
  static _toHtmlTable(playlistTracks) {
    if (playlistTracks.length === 0) {
      return '<p>None</p>';
    }

    const tbody = playlistTracks.map((playlistTrack) => `    <tr>
      <td>${playlistTrack.trackId}</td>
      <td>${playlistTrack.trackTitle}</td>
      <td>${playlistTrack.artistId}</td>
      <td>${playlistTrack.artistName}</td>
      <td>${playlistTrack.albumId}</td>
      <td>${playlistTrack.albumTitle}</td>
    </tr>`)
      .join('\n');

    return `<table>
  <thead>
    <tr>
      <th>Track ID</th>
      <th>Track Title</th>
      <th>Artist ID</th>
      <th>Artist Name</th>
      <th>Album ID</th>
      <th>Album Title</th>
    </tr>
  </thead>
  <tbody>
${tbody}
  </tbody>
</table>`;
  }

}

module.exports = PlaylistMailer;
