const Apify = require('apify');

const logger = require('./logger');
const PlaylistHistorySettings = require('./PlaylistHistorySettings');
const PlaylistMetadata = require('./PlaylistMetadata');

class PlaylistMetadataStore {

  constructor(playlistId) {
    this._playlistId = playlistId;
    this._metadata = null;
  }

  async load() {
    this._metadata = await PlaylistMetadataStore._loadMetadata(this.storeName());
  }

  entries() {
    this._checkDataHasBeenLoaded();
    return this._metadata['items'].map(i => new PlaylistMetadata(i));
  }

  // WARNING:
  // - total: Total count of entries in the dataset.
  // - count: Count of dataset entries returned in this set.
  entriesCount() {
    this._checkDataHasBeenLoaded();
    return this._metadata['count'];
  }

  hasEntry() {
    this._checkDataHasBeenLoaded();
    return this.entriesCount() > 0;
  }

  lastEntry() {
    this._checkDataHasBeenLoaded();
    if (this.hasEntry()) {
      return this.entries()[this.entriesCount() - 1];
    }
    return null;
  }

  storeName() {
    return `${PlaylistHistorySettings.STORE_PREFIX}--${this._playlistId}--metadata`;
  }

  async save(playlistMetadata) {
    logger.debug("Saving metadata to '%s': %j", this.storeName(), playlistMetadata.toJSON());
    const metadataStore = await Apify.openDataset(this.storeName());
    await metadataStore.pushData(playlistMetadata.toJSON());
    logger.debug("Saved metadata to '%s'", this.storeName());
  }

  static async _loadMetadata(metadataStoreName, itemCount = 3) {
    logger.debug("Loading metadata from '%s'", metadataStoreName);
    logger.debug('Opening dataset');
    const metadataStore = await Apify.openDataset(metadataStoreName);
    logger.debug('Dataset opened');
    logger.debug('Retrieving dataset information');
    const metadataInfo = await metadataStore.getInfo();
    logger.debug('Dataset information retrieved');
    const offset = Math.max(0, metadataInfo['itemCount'] - itemCount);
    logger.debug('Loading metadata with offset %d', offset);
    const metadata = await metadataStore.getData({offset: offset});
    logger.debug('Loaded metadata: ' + JSON.stringify(metadata, null, 2));
    return metadata;
  }

  _checkDataHasBeenLoaded() {
    if (this._metadata === null) {
      throw new Error('Data has not been loaded!');
    }
  }

}

module.exports = PlaylistMetadataStore;
