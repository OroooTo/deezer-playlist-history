const Ajv = require('ajv').default;
const fs = require('fs');
const path = require('path');

class PlaylistMetadataSchema {

  constructor() {
    this._ajv = new Ajv({allErrors: true});
    this._validate = this._ajv.compile(PlaylistMetadataSchema._schema);
  }

  validate(data) {
    const valid = this._validate(data);

    if (!valid) {
      throw new Error(`Invalid data: ${this._ajv.errorsText(this._validate.errors)}`);
    }
  }

  static get _schema() {
    const schemaPath = path.join(__dirname, 'PlaylistMetadata-schema.json');
    const schemaAsJSON = fs.readFileSync(schemaPath, {encoding: 'utf-8'});
    return JSON.parse(schemaAsJSON);
  }

}

module.exports = PlaylistMetadataSchema;
